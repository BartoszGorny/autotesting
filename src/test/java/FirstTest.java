import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;

public class FirstTest extends TestBase {


    @Test
    public void searchWithFirstElement() {
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.id("lst-ib"));
        searchInput.sendKeys("Adam Małysz");
        List<WebElement> listOfWebElem = driver.findElements(By.className("lsb"));
        System.out.println("Kliknąłem przycisk " + listOfWebElem.get(0).getAttribute("value"));
        listOfWebElem.get(0).click();
        WebElement searchResult = driver.findElement(By.cssSelector("span[data-original-name='Adam Małysz']"));

    }

    @Test
    public void searchWithSecondElement() {
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.cssSelector("input[title='Szukaj']"));
        searchInput.sendKeys("Adam Małysz");
        List<WebElement> listOfWebElem = driver.findElements(By.className("lsb"));
        System.out.println("Kliknąłem przycisk " + listOfWebElem.get(1).getAttribute("value"));

        listOfWebElem.get(1).click();
        //WebElement searchResult = driver.findElement(By.cssSelector("span[data-original-name='Adam Małysz']"));
    }

}
