import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Registration extends TestBase {

    @Test
    public void demoQaTest() {
        driver.get("http://demoqa.com/registration/");

        selectName();
        selectCountry();
        selectCheckboxes();
        selectDate();
        selectInformation();
        setDescription();
        setPassword();
        //   WebElement clickButton = driver.findElement(By.cssSelector("input[name='pie_submit']"));
        //   clickButton.click();
        //   Assert.assertTrue(clickButton.isDisplayed());
    }

    private WebElement getRandomElement(List<WebElement> elementList) {
        Random random = new Random();
        return elementList.get(random.nextInt(elementList.size() - 1));
    }

    private void getPrintSelectedElements(List<WebElement> list) {
        for (WebElement isTrue : list) {
            if (isTrue.isSelected()) {
                System.out.println("checkbox " + isTrue.getAttribute("value") + " jest zaznaczony");
            } else {
                System.out.println("checkbox " + isTrue.getAttribute("value") + " nie jest zaznaczony");

            }
        }

    }

    public void selectName() {
        WebElement firstNameInput = driver.findElement(By.cssSelector("input[name='first_name']"));
        firstNameInput.sendKeys("TestFirstName");

        WebElement lastNameInput = driver.findElement(By.cssSelector("input[name='last_name']"));
        lastNameInput.sendKeys("TestLastName");
    }

    public void selectCheckboxes() {
        List<WebElement> materialStructures =
                driver.findElements(By.cssSelector("input[class='input_fields  radio_fields']"));
        WebElement element = getRandomElement(materialStructures);
        element.click();

        getRandomElement(materialStructures).click();
        List<WebElement> hobbies = driver.findElements(By.cssSelector("input[class='input_fields  piereg_validate[required] radio_fields']"));
        hobbies.get(0).click();
        hobbies.get(2).click();
        getPrintSelectedElements(hobbies);
        Assert.assertFalse(hobbies.get(1).isSelected());
    }

    public void selectCountry() {
        Select countries = new Select(driver.findElement(By.cssSelector("select[id='dropdown_7']")));
        countries.selectByValue("Poland");
        Assert.assertEquals(countries.getFirstSelectedOption().getText(), "Poland");
    }

    public void selectDate() {
        Select month = new Select(driver.findElement(By.cssSelector("select[id='mm_date_8']")));
        month.selectByIndex(1);
        Assert.assertEquals(month.getFirstSelectedOption().getText(), "1");


        Select day = new Select(driver.findElement(By.cssSelector("select[id='dd_date_8']")));
        day.selectByIndex(20);
        Assert.assertEquals(day.getFirstSelectedOption().getText(), "20");


        Select year = new Select(driver.findElement(By.cssSelector("select[id='yy_date_8']")));
        year.selectByValue("1999");


        Assert.assertEquals(year.getFirstSelectedOption().getText(), "1999");
    }

    public void selectInformation() {

        sendKeys(By.cssSelector("input[id='phone_9']"), "921234575");
        sendKeys(By.cssSelector("input[id='username']"), "Admin" + getRandomNumber());
        sendKeys(By.cssSelector("input[id='email_1']"), "mail" + getRandomNumber());

    }

    public void setDescription() {
        sendKeys(By.cssSelector("input[id='profile_pic_10']"), "C:\\tools\\Przechwytywanie.png");
        WebElement aboutInput = driver.findElement(By.id("description"));
        aboutInput.sendKeys("Loren ipsum loorem ipsum:");


    }

    public void setPassword() {
        sendKeys(By.cssSelector("input[id='password_2']"), "zaq12wsx");
        sendKeys(By.cssSelector("input[id='confirm_password_password_2']"), "zaq12wsx");
    }

    public void sendKeys(By selector, String textToSet) {
        WebElement confirmPass = driver.findElement(selector);
        confirmPass.sendKeys(textToSet);
    }

    public int getRandomNumber() {
        return ThreadLocalRandom.current().nextInt(1000, 9999);
    }
}


